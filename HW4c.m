A = randperm(2^16-1,2^16-1);
tic();
fid = fopen('matlabsql.sql','w');
fprintf(fid,'PRAGMA foreign_keys=OFF;\n');
% Why STRANGE?
fprintf(fid,'BEGIN TRANSACTION; CREATE TABLE STRANGE(ID INT PRIMARY KEY NOT NULL);');
for i = 1:length(A)
fprintf(fid,'INSERT INTO "STRANGE" VALUES(%d);\n',A(i));
end
fprintf(fid,'COMMIT;\n');
fclose(fid);
stop = toc();
fprintf('Time [s] to write binary = %.8f.',stop);
