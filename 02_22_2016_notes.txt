Reported std. deviation from last time, as well as std. error, from the whole class. Discussed the phenomenon of a training curve.

Set up linear regression in Matlab and Excel. Results of fit matched each other, as well as calculation of standard uncertainty from
the line. 
S_res = sqrt(sum(y-y-hat)^2/(N-2)), where y-hat is the values of y when the same x's are stuck into the linear regression formula, and
y is the original data.

Type B error is due to error from the measurement process.
Always write down the model of machine you are working on. Note the specifications.
In the case of the reaction time data, we assume a rectangular distribution. We scale the error according to a confidence interval.
For instance, when we combine a gaussian and a rectangular distribution, we scale the Gaussian and rectangular standard deviations
 to give a predetermined confidence interval, say 95%.