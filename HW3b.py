# See http://bobweigel.net/cds302/index.php?title=RDMS#Activity
# For how to do (a) through (c) with a single program.

#head -1000 astro.csv > astro1000.csv
#python HW3b.py > astro1000.sql
#time ./sqlite3 astro1000.db < astro1000.sql

import csv

print 'PRAGMA foreign_keys=OFF;'
print 'BEGIN TRANSACTION;'
print 'CREATE TABLE ASTRO (Preview text, Collection text, Obs_ID text);'

with open('astro1000.csv','r') as csvfile:
     contents = csv.reader(csvfile, delimiter=',')
     for row in contents:
         print "INSERT INTO ASTRO (Preview, Collection, Obs_ID) VALUES ('" + row[0] + "','" + row[1] + "','" + row[2] + "');"

print 'COMMIT;'
