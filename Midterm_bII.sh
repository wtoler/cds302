# Bash program that runs 20 times and waits 5 seconds
 # after each run.
 
 # Remove existing log file.
 rm -f repeater.log
 # Initialize a counter variable.
 COUNTER=0
 # Run commands in while loop 20 times
 # (-lt mean less than in Bash)
 while [ $COUNTER -lt 20 ]; do
 	 echo "Starting iteration $COUNTER"
     let COUNTER=$COUNTER+1
     # Assign the ouptut of the command
     # date +'%Y %m %d %H %M %S.%N'
     # to variable START
     START=$(date +'%Y %m %d %H %M %S.%N')
     # Display the value of the variable START
     echo "$START"
 
     ps aux --sort=-pcpu | perl -pe 's/ +/,/g' | head -11 | tail -10
     ps aux --sort=-pcpu| perl -pe 's/ +/,/g'| head -11| tail -10| cut -f 3,4,11 -d',' >>   	repeater.log
     sleep 5
 done