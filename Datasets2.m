%This data was taken with a stopwatch. The goal was to stop the watch as
%close as possible to 2 seconds.
reaction_data = [1.81; 1.94; 1.90; 2.03; 1.97;
    1.94; 1.93; 1.90; 1.94; 1.90; 2.07; 1.93;
    2.06; 1.90; 2.00] %in seconds
%Paper was dropped from shoulder height, time to hit floor was recorded.
paper_data = [2.06;1.68;1.72;2.03;2.69;1.65;2.00;
    1.97;1.87;1.87;1.84;1.31;1.62;2.06;1.78;1.65;
    2.06;2.13;1.69;1.87] %in seconds

%30 100-ohm (nominally) resistors were measured, results were recorded.
resistor_data = [99.4;98.1;98.8;99.2;96.2;98.4;98.5;97.6;96.5;98.7;
    96.3;98.8;99.8;97.4;99.8;97.8;100.2;98.3;97.4;98.4;
    98.4;117.9;99.6;99.6;97.5;98.2;98.3;98.1;99.6;96.1] %in ohms
%Five cords were measured.
cord_length = [12.043;12.027;12.039;12.075;18.715] %in meters
time_difference = [49.00;47.00;48.00;48.00;82.00] %in nanoseconds
short_cord = 3.062

% All values are inputed in the following order: Brass, aluminum, copper,
% steel(black), acrylic, nylon, PP,PVC, ironwood, pine, oak, poplar.

%length measure in cm
cube_analog_measure = [2.52,2.53,2.52; %Brass
    2.50,2.51,2.50; %aluminum
    2.52,2.53,2.52; %copper
    2.51,2.52,2.54; %steel(black)
    2.54,2.55,2.55; %acrylic
    2.54,2.54,2.51; %nylon
    2.52,2.53,2.53; %pp
    2.51,2.52,2.55; %pvc
    2.45,2.46,2.42; %ironwood
    2.54,2.51,2.50; %pine
    2.51,2.54,2.52; %oak
    2.48,2.53,2.45] %poplar

%length measure in mm
cube_digital_measure = [25.40,25.39,25.32; %Brass
    25.28,25.30,25.31; %aluminum
    25.50,25.45,25.44; %copper
    25.44,25.63,25.36; %steel(black)
    25.61,25.44,25.56; %acrylic
    25.11,25.08,25.29; %nylon
    25.39,25.49,25.44; %pp
    25.54,25.43,25.37; %pvc
    24.55,24.81,24.88; %ironwood
    25.21,25.50,25.22; %pine
    25.47,25.19,25.16; %oak
    24.64,25.47,24.64] %poplar

%length measure in mm, assumed accuracy to +- 0.5 mm
cube_ruler_measure = [25.0,25.5,25.0; %Brass
    25.0,25.0,25.5; %aluminum
    25.0,25.5,25.0; %copper
    25.5,25.5,25.5; %steel(black)
    25.5,26.0,26.0; %acrylic
    25.5,25.5,25.0; %nylon
    25.5,25.0,25.0; %pp
    25.0,25.5,25.5; %pvc
    24.5,25.0,24.5; %ironwood
    25.5,25.0,25.0; %pine
    24.5,25.0,25.0; %oak
    24.5,25.5,25.0] %poplar
% Mass, measured in grams with a spring scale. Values must have 50
% subtracted from them to account for the stand.
spring_mass = [190;92;197;178;69;62;65;71;67;58;60;60]
reaction_index = transpose(1:length(reaction_data))
reaction_std = std(reaction_data)
reaction_std_error = reaction_std/sqrt(size(reaction_data,1))
reaction_runmean = cumsum(reaction_data)./transpose(1:length(reaction_data))
plot(reaction_index,reaction_data,'b.','MarkerSize',10)
hold on;
grid on;
plot(reaction_index,reaction_runmean,'r.','MarkerSize',10)
title('Reaction Time Analysis')
xlabel('Trial #')
ylabel('Time (s)')
legend('Data','Running average')

%Linear regression
reaction_trial_num = transpose([1:15])
%Hardcode polyfit alternative
A = horzcat(reaction_trial_num,ones(15,1))
lin_result = (transpose(A)*A)\transpose(A)*reaction_data

[p,S] = polyfit(reaction_trial_num, reaction_data,1)
[y,delta] = polyval(p,reaction_trial_num,S)
N  = 15
S_res = sqrt(sum((reaction_data-y).^2)/(N-2))