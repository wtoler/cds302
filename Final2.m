%This code is for questions 3, and 4.

% This is the file I was using.
%!curl -O http://mag.gmu.edu/git-data/cds302/MassVehicleCensusData/rae_public_noheader.csv

%Question 3
!cat rae_public_noheader.csv | cut -d ',' -f25 > mpg.txt
mpg = dlmread('mpg.txt','\n'); %dlmread replaces blank lines with 0 when I used \n as delimiter
mpg(mpg==0)=NaN;

!cat rae_public_noheader.csv | cut -d ',' -f23 > curb_weight.txt
curb_weight = dlmread('curb_weight.txt','\n');
curb_weight(curb_weight==0)=NaN;
c = corrcoef(mpg,curb_weight,'rows','pairwise')
%correlation is -0.4820.
% Not sure why your correlation is this.  I just ran (again) and got -0.778

%Question 4
model_year = dlmread('model_year.txt'); %This time we don't care if the values are run together
ave = mean(model_year);
%Average value is about 2002.7
