%This code downloads a year of daily videos from the Solar Dynamics Observatory

clear;
tic();
days = 365; % # of days we are downloading/analyzing
counter = 0;
dno = datenum(2014,1,1);
for i = dno:dno+days-1
  dvo = datevec(i);
  counter = counter + 1;
  
url = sprintf('http://sdo.gsfc.nasa.gov/assets/img/dailymov/%d/%02d/%02d/%d%02d%02d_1024_0171.mp4',...
    dvo(1),dvo(2),dvo(3),dvo(1),dvo(2),dvo(3));
filename = sprintf('%d%02d%02d_1024_0171.mp4',dvo(1),dvo(2),dvo(3));
outfilename = urlwrite(url,filename)
end
texecute= toc()