% Compiles into montage
fileID = fopen('solar4.sh','w');
fprintf(fileID,'montage -tile 13X27');

dno = datenum(2014,1,1);
for i = dno:dno+350
    dvo = datevec(i); 
    name = sprintf('%d%02d%02d_1930_hmiigr_512.jpg',dvo(1),dvo(2),dvo(3))
    dinfo = dir(name);
     if (dinfo.bytes < 1000)
         name = 'null:';
     end
     fprintf(fileID,' %s', name);
end
fprintf(fileID,'solar3.jpg');
fclose(fileID);