tic();                                                                          
fid = fopen('file.bin','rb');                                                   
% I find it strange that this works
Aread = fread(fid,'double');                                                    
% According to the documentation, you should have written
% data = fread(fif,Inf,'double');
% Not sure why MATLAB does not document the fact that if the number of values to read
% in is not specified, it defaults to all (equivalent to Inf).
fclose(fid);                                                                    
stop = toc();                                                                    
fprintf('Number of values = %d.Time [s] to read binary  = %.8f\n',...
    length(Aread),stop);
