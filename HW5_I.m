% See http://bobweigel.net/cds302/index.php?title=HW5
% for alternative methods of doing this.
clear;
s = '1000,100,10,1,X'
s = regexprep(s,'X','-1');
sn = str2num(s)

clear;
s = '### 1000,100,10,1 ZZZ'
s = regexprep(s,'#','');
s = regexprep(s,' ','');
s = regexprep(s,'Z','');
sn = str2num(s)

clear;
s = '### 1000,100,10,1 ###'
s = regexprep(s,'#','');
sn = str2num(s)

clear;
s = '### 1000,100,10,1 xxx'
s = regexprep(s,'#','');
s = regexprep(s,'x','');
sn = str2num(s)
