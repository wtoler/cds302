wget http://mag.gmu.edu/git-data/cds302/MassVehicleCensusData.zip
unzip -d . MassVehicleCensusData.zip

#Answer to question 1: 5206546 unique vehicles in database (given by line below)
cat rae_public_noheader.csv | cut -d ',' -f3 | sort | uniq | wc -l
# Should have used vehicle ID - from metadata: "because plates can be transferred from one vehicle to another, this ID may be associated with multiple Vehicle IDs. "

#Answer to question 2: 236 unique vehicle makes (given by line below).
cat rae_public_noheader.csv | cut -d ',' -f18 | sort | uniq | wc -l


#Need these files for MATLAB analysis later
cat rae_public_noheader.csv | cut -d ',' -f17 > model_year.txt
cat rae_public_noheader.csv | cut -d ',' -f23 > curb_weight.txt
cat rae_public_noheader.csv | cut -d ',' -f25 > mpg.txt