clear;
url = 'http://lasp.colorado.edu/data/sorce/tsi_data/daily/sorce_tsi_L3_c24h_latest.txt';
tline = 'irradiance_data.txt';
if ~exist(tline,'file') % If file is not on disk, download it.
    urlwrite(url,tline);
end

fid = fopen(tline,'r');
k = 1;
while 1
    tline2 = fgetl(fid);
    if ~ischar(tline2),break,end;
    if (length(tline2) >= 4 & tline2(1:2) == '20')  %Get all the data from 2015
        data(k,:) = str2num(tline2);
        k = k+1;
    end
end
fclose(fid);
dno = datenum(2003,2,25); %Data data began to be taken in this file
dates = [dno:dno+length(data)-1];  %Create dates vector

data(data(:,5)==0,5) = NaN; %Replace any zeroes in data with NaN.
data(:,1) = floor(data(:,1)); %Trim off decimal in date

figure(1)
plot(dates,data(:,5))    % Note that missing values (NaN) are not displayed.
hold on;
grid on;
title('Plot of SORCE data','FontSize',15)
xlabel('Time (years)')
ylabel('Irradiance (W/m^2)')
datetick('x','yyyy','keepticks')    %Something's wrong with dateticks, not displaying years properly
xlim([731637 736048])
hold off;