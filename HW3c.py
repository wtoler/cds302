#head -10000 astro.csv > astro10000.csv
#python HW3c.py > astro10000.sql
#time ./sqlite3 astro10000.db < astro10000.sql
import csv

print 'PRAGMA foreign_keys=OFF;'
print 'BEGIN TRANSACTION;'
print 'CREATE TABLE ASTRO (Preview text, Collection text, Obs_ID text);'

with open('astro10000.csv','r') as csvfile:
     contents = csv.reader(csvfile, delimiter=',')
     for row in contents:
         print "INSERT INTO ASTRO (Preview, Collection, Obs_ID) VALUES ('" + row[0] + "','" + row[1] + "','" + row[2] + "');"

print 'COMMIT;'
