#head -100 astro.csv > astro100.csv
#python HW3a.py > astro100.sql
#time ./sqlite3 astro100.db < astro100.sql

import csv

print 'PRAGMA foreign_keys=OFF;'
print 'BEGIN TRANSACTION;'
print 'CREATE TABLE ASTRO (Preview text, Collection text, Obs_ID text);'

with open('astro100.csv','r') as csvfile:
     contents = csv.reader(csvfile, delimiter=',')
     for row in contents:
         print "INSERT INTO ASTRO (Preview, Collection, Obs_ID) VALUES ('" + row[0] + "','" + row[1] + "','" + row[2] + "');"

print 'COMMIT;'

# Note that for speed, you could do someting like
# for row in contents:
#    txt = txt+"INSERT INTO ASTRO (Preview, Collection, Obs_ID) VALUES ('" + row[0] + "','" + row[1] + "','" + row[2] + "');
# print txt
# This way, the write is executed only once.  This requires more memory, however.  This is called "buffering",
# and modern hard drives actually do a bit of this for you.  Before it does a long write, waits until it has
# a lot to do. Operating systems also do buffering.
