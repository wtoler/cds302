%Script for N=16 case.

rm -f out*.txt
rm -f tmp*.txt
echo "Starting Serial Run (N=16)"
octave --silent --eval "HW10" > out1.txt
octave --silent --eval "HW10" > out2.txt
octave --silent --eval "HW10" > out3.txt
octave --silent --eval "HW10" > out4.txt
octave --silent --eval "HW10" > out5.txt
octave --silent --eval "HW10" > out6.txt
octave --silent --eval "HW10" > out7.txt
octave --silent --eval "HW10" > out8.txt
octave --silent --eval "HW10" > out9.txt
octave --silent --eval "HW10" > out10.txt
octave --silent --eval "HW10" > out11.txt
octave --silent --eval "HW10" > out12.txt
octave --silent --eval "HW10" > out13.txt
octave --silent --eval "HW10" > out14.txt
octave --silent --eval "HW10" > out15.txt
octave --silent --eval "HW10" > out16.txt

alldone="no"
while [ "$alldone" = "no" ]; do
    echo "Checking if all files exist."
    # Note that the spaces after [[ and before ]] are required in the next line.
    if [[ -s out1.txt && -s out2.txt && -s out3.txt && -s out4.txt && -s out5.txt && -s out6.txt && -s out7.txt && -s out8.txt && -s out9.txt && -s out10.txt && -s out11.txt && -s out12.txt && -s out13.txt && -s out14.txt && -s out15.txt && -s out16.txt ]]; then
	echo "All files exist.  Concatenating into out.txt"
	cat out1.txt out2.txt out3.txt out4.txt out5.txt out6.txt out7.txt out8.txt out9.txt out10.txt out11.txt out12.txt out13.txt out14.txt out15.txt out16.txt > outa.txt
	alldone="yes"
    fi
done