% Note that only 9 images are compiled into a montage; the desired image for 01/08/2014 does not exist on the browser.
% File should be named HW6_IV.m
fileID = fopen('solar2.sh','w');
fprintf(fileID,'montage -tile +10+1 ');
for i = 1:10
    fprintf(fileID,'201401%02d_1930_hmiigr_512.jpg ',i);
end
fprintf(fileID,'solar2.jpg');
fclose(fileID);
type solar2.sh
