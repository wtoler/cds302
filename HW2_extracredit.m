fid = fopen('matlabsql.sql','w');
fprintf(fid,'PRAGMA foreign_keys=OFF;\n');
fprintf(fid,'BEGIN TRANSACTION;CREATE TABLE NUMBERS(ID INT PRIMARY KEY NOT NULL,ID2 INT NOT NULL);');
for i = 1:500
fprintf(fid,'INSERT INTO "NUMBERS" VALUES(%d,%d);\n',i,501-i);
end
fprintf(fid,'COMMIT;\n');
fclose(fid);

% Note that you could have avoided the use of a for loop using vectorization:
% x = [1:500];
% y = [500:-1:1];
% z = [x;y];
% fprintf('INSERT INTO "NUMBERS" VALUES(%d,%d);\n',z(:));
% If the length of x and y were much larger, you would notice a significant
% speed-up when using this approach.  (But the for loop is easier to read and
% thus preferred in this case.)

