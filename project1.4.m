% Better to keep all options near the top of a script.
debug = false;
clf;
x1 = 5;
x2 = 0;
y2 = 6;
p1 = 5;
p2 = 6;
p3 = 3;
gamma = pi/4;
L1 = 3;
L2 = 3*sqrt(2);
L3 = 3;

%Graph function
x = linspace(-pi,pi,1000);
% Not a good idea to use "f" as it looks like something that you would name a 
% scalar not a function.
y = f(x);
figure(1)
plot(x,y)
grid on


%Find zeros: Choose starting values by inspection of figure(1)
theta = fzero(@f,...
2.5); %this number is changed to find all the roots

% theta = -0.9763, 0.1342, 1.0398, 2.3540
theta

A2 = L3.*cos(theta) - x1;
B2 = L3.*sin(theta);
A3 = L2.*(cos(theta).*cos(gamma)- sin(theta).*sin(gamma)) - x2;
B3 = L2.*(cos(theta).*sin(gamma) + sin(theta).*cos(gamma)) - y2;
N1 = B3.*(p2.^2-p1.^2 - A2.^2 - B2.^2) - B2.*(p3.^2 - p1.^2 - A3.^2 - B3.^2);
N2 = -A3.*(p2.^2 - p1.^2 - A2.^2 - B2.^2) + A2.*(p3.^2 - p1.^2 - A3.^2 - B3.^2);
D = 2.*(A2.*B3 - B2.*A3);
u1 = N1/D;
u2 = u1+L2*cos(theta+gamma);
v1 = N2/D;
v2 = v1 + L2*sin(theta + gamma);
u3 = u1 + L3*cos(theta);
v3 = v1 + L3*sin(theta);

figure(2)
%Plot triangle platform
plot([u1 u2 u3 u1],[v1 v2 v3 v1],'r','LineWidth',4);
hold on;

%Plot struts
plot([0 x1 x2],[0 0 y2],'bo');
plot([0 u1],[0 v1],'LineWidth',2);
plot([x2 u2],[y2 v2],'LineWidth',2);
plot([x1 u3],[0 v3],'LineWidth',2);
hold off;

%Double-check strut lengths (uncomment to display)
if (debug)
    p1
    p1calc = sqrt(u1^2+v1^2)
    p2
    p2calc = sqrt((u3-x1)^2 + (v3 - 0)^2)
    p3
    p3calc = sqrt((u2-x2)^2+(v2-y2)^2)
end
