%Import file created by HW5read.sh
clear;
fid=fopen('HW5_trimmed.txt');
k = 1;
while 1
  tline = fgetl(fid);
  if ~ischar(tline)
    break;
  end
    tline = regexprep(tline,'-',' ');
    tline = regexprep(tline,':',' ');
    % Convert line to array and place array in row of matrix.
    A(k,:) = str2num(tline); 
    k = k+1;
end
fclose(fid);
whos A