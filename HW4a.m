clear;                                                                          

A = randi([0 2^16-1],10^5,1);

tic();                                                                          
save -ascii -double file.txt A                                                  
stop = toc();                                                                                                                         
fprintf('Time [s] to write ASCII  = %.8f.',stop); 