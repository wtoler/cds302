clear;
url = 'http://lasp.colorado.edu/data/sorce/tsi_data/daily/sorce_tsi_L3_c24h_latest.txt';
tline = 'irradiance_data.txt';
if ~exist(tline,'file') % If file is not on disk, download it.
    urlwrite(url,tline);
end

fid = fopen(tline,'r');
k = 1;
while 1
    tline2 = fgetl(fid);
    if ~ischar(tline2),break,end;
    if (length(tline2) >= 4 & tline2(1:4) == '2014')  %Get all the data from 2015
        data(k,:) = str2num(tline2);
        k = k+1;
    end
end
fclose(fid);

plot(90:365,data(90:365,5))
hold on;
title('Plot of SORCE data','FontSize',15)
xlabel('Day of the year')
ylabel('Irradiance (W/m^2)')