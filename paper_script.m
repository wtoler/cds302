clear;
clf;
%Paper was dropped from shoulder height, time to hit floor was recorded.
paper_data = [2.06;1.68;1.72;2.03;2.69;1.65;2.00;
    1.97;1.87;1.87;1.84;1.31;1.62;2.06;1.78;1.65;
    2.06;2.13;1.69;1.87] %in seconds

%Calculate running mean of data
paper_runmean = cumsum(paper_data)./transpose(1:length(paper_data))
%Trial number
paper_index = transpose(1:length(paper_data))
plot(paper_index,paper_data,'b.','MarkerSize',10)
hold on;
grid on;
plot(paper_index,paper_runmean,'r.','MarkerSize',10)
title('Paper Time Analysis')
xlabel('Trial #')
ylabel('Time (s)')
legend('Data','Running average')

[p,S] = polyfit(paper_index, paper_data,1)
[y,delta] = polyval(p,paper_index,S)

N  = 20
%Total standard deviation
S_res = sqrt(sum((paper_data-y).^2)/(N-2))

%Standard deviation, sans the type B error.
delta = y - mean(paper_data)
%FINISH UP NEXT TIME.