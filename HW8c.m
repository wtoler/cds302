clearvars -except dates data

days = 365; % # of days we are analyzing
ct = 0;
dno = datenum(2014,1,1);
for t = dno:dno+days-1
    ct = ct+1;
    dvo = datevec(t);
    filename = sprintf('%d%02d%02d_1024_0171.mp4',dvo(1),dvo(2),dvo(3));
    obj = VideoReader(filename);
    vid = read(obj);
    frames = obj.NumberOfFrames;
    counter = frames;
    cutoff=1e8;
    
    sumImage = double(vid(:,:,:,1)); % Initialize to first image.
    for k=2:frames % Read in remaining images.
        rgbImage = double(vid(:,:,:,k));
        mini = min(rgbImage((512-400):(512+400),(512-400):(512+400),:));
        if mini == 0               %If there any black pixels in a big square in the center
            counter = counter - 1; %Ignore frame
        else
            sumImage = sumImage + rgbImage;
        end
        
    end;
    
    meanImage = sumImage/counter;
    meanSum(ct) = sum(meanImage(:));  %Do not erase this vector; need it for plot in HW8d.m
    
    %This text will eventually go to a log file
    if counter ~= frames
        fprintf('Ignored %d frames in %s\n',frames - counter,filename)
    end
    
    imwrite(uint8(meanImage),sprintf('%d%02d%02d_1024_0211.png',dvo(1),dvo(2),dvo(3)))
end
dlmwrite('average_intensity.txt',sumImage);