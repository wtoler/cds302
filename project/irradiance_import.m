url = 'http://lasp.colorado.edu/data/sorce/tsi_data/daily/sorce_tsi_L3_c24h_latest.txt';
tline = 'irradiance_data.txt';
if ~exist(tline,'file') % If file is not on disk, download it.
    urlwrite(url,tline);
end

fid = fopen(tline,'r');
k = 1;

%Get the data from 2011-2014
while 1
    tline2 = fgetl(fid);
    if ~ischar(tline2),break,end;
    if (length(tline2) >= 4 & (tline2(1:4) == '2011' | tline2(1:4) == '2012'|...
            tline2(1:4) == '2013'| tline2(1:4) == '2014'))
        data(k,:) = str2num(tline2);
        k = k+1;
    end
end
fclose(fid);

data(data(:,5)==0,5) = NaN; %Replace any zeroes in data with NaN.
figure(1)
J = data(:,5);
plot(J)    % Note that missing values (NaN) are not displayed.
hold on;
grid on;
title('Plot of SORCE TSI data, 2011-2014','FontSize',15)
xlabel('Time (days)')
ylabel('Irradiance (W/m^2)')
hold off;