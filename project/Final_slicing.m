%clearvars -except data dates
days = 365*4+1; % # of days we are analyzing
dno = datenum(2011,1,1);
counter =0;
count = 0;
%for s = 100:100:300      %Uncomment this for-loop to vary the slice size
    %count = count + 1;
    s=200;
    for t = dno:dno + days -1
        
        counter = counter + 1;
        dvo = datevec(t);
        
        obj = imread(sprintf('%d%02d%02d_1024_0304.png',dvo(1),dvo(2),dvo(3)));
        if size(obj,3) == 1 %White images only have one color channel
            A(counter) = NaN;
        else
            [X,Y] = meshgrid(1:1024,1:1024);
            for l = 1:3
                tmp = obj(:,:,l);
                Is = find( (X-512).^2 + (Y-512).^2 < 420^2);
                tmp(Is) = 0;
                obj(:,:,l) = tmp;
            end
            A(counter) = sum(obj(:));
            A(counter)=sum(sum(sum(obj(1:s,:,:))));
            
            %Uncomment to display image
%             obj(201:end,:,:) = 0;
%             imagesc(obj)
%             break
        end
        counter
    end
    %Luminance(count,:) = A; 
    %counter = 0;
%end