days = 365*4+1; % # of days we are analyzing
dno = datenum(2011,1,1);
counter =0;
for t = dno:dno + days -1
    
    counter = counter + 1;
    dvo = datevec(t);
    
    obj = imread(sprintf('%d%02d%02d_1024_0304.png',dvo(1),dvo(2),dvo(3)));
    Img = obj;
    
    if size(obj,3) == 1 %White images only have one color channel
        wes(:,counter) = NaN(1024,1);
    else
        GrayImage = uint8(0.2989 * Img(:,:,1) + 0.5870 * Img(:,:,2) + ...
        0.1140 * Img(:,:,3));
        wes(:,counter) = GrayImage(:,512); %Vertical strip
    end
   counter
end
imagesc(wes)
colormap