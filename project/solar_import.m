%This code downloads daily videos from the Solar Dynamics Observatory

days = 365; % # of days we are downloading/analyzing
counter = 0;
dno = datenum(2011,7,25);
for i = dno:dno + days-1
    dvo = datevec(i);
    counter = counter + 1;
    
    url = sprintf('http://sdo.gsfc.nasa.gov/assets/img/dailymov/%d/%02d/%02d/%d%02d%02d_1024_0304.mp4',...
        dvo(1),dvo(2),dvo(3),dvo(1),dvo(2),dvo(3));
    filename = sprintf('%d%02d%02d_1024_0304.mp4',dvo(1),dvo(2),dvo(3));
    outfilename = urlwrite(url,filename)
end