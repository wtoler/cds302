clear;
tic();

days = 1; % # of days we are downloading/analyzing

dno = datenum(2014,1,1);
for i = dno:dno+days-1 % Some entries are missing in our TSI dataset;
                       % We don't really want all videos in the year
  dvo = datevec(i); 
  
url = sprintf('http://sdo.gsfc.nasa.gov/assets/img/dailymov/%d/%02d/%02d/%d%02d%02d_1024_0193.mp4',...
    dvo(1),dvo(2),dvo(3),dvo(1),dvo(2),dvo(3));
filename = sprintf('video_%02d.mp4',i+1-dno);
outfilename = urlwrite(url,filename)
end

obj = VideoReader('video1.mp4');
vid = read(obj);
frames = obj.NumberOfFrames;

sumImage = double(vid(:,:,:,1)); % Initialize to first image.
for i=2:frames % Read in remaining images.
  rgbImage = double(vid(:,:,:,i));
  sumImage = sumImage + rgbImage;
end;
meanImage = sumImage/frames;

%Code to make Black Spot in image
for i = 1:1024
    for j = 1:1024
        r = sqrt((i-1024/2)^2 + (j-1024/2)^2);
        if r <= 396
            meanImage(i,j,:) = 0;
        end
    end
end
% writerObj = VideoWriter('things.mp4','MPEG-4');
% writerObj.FrameRate = 10;
% open(writerObj);
% writeVideo(writerObj,vid)
% close(writerObj)
imwrite(uint8(meanImage),'processed4.png')
texpired= toc()