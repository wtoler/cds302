urlbase = 'http://mag.gmu.edu/git-data/cds302/';

  fname = sprintf('bou2013040%dvmin.min',2);
  url = sprintf('%s/%s',urlbase,fname);
  fprintf('Downloading and saving %s ...',fname);
  system(sprintf('curl -O %s',url));
  fprintf('Done\n');

  fprintf('Reading %s ...',fname);  
  fid=fopen(fname);
  k = 1;
  while 1
    tline = fgetl(fid);
    if ~ischar(tline),break;end
    if strmatch('2013-',tline)
      tline = regexprep(tline,'-',' ');
      tline = regexprep(tline,':',' ');
      % Convert line to array and place array in row of matrix.
      A(k,:) = str2num(tline); 
      k = k+1;
    end
  end
  fclose(fid);
  fprintf('Done.\n');  

  fname2 = sprintf('bou2013040%dvmin.mat',i);
  fprintf('Saving %s ...',fname2);  
  save(fname2,'A');
  fprintf('Done.\n'); 