% Note that this is the slow way of doing it.  Faster do to
% fprintf(...,A') without for loop
%This program assumes A is still in memory from HW5read.m
fileID = fopen('HW5write.txt','w');
for i = 1:size(A)
    fprintf(fileID,'%d-%02d-%02d,%02d,%02d:%.3f,%d,%.2f,%.2f,%.2f,%.2f\n',A(i,:));
end
fclose(fileID);
type HW5write.txt
