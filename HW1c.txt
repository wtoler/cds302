I chose wunderground.com as my sample database. The first link has "descriptive forecasts," or text summaries, i.e. 
"Tuesday Night 0% Precip. / 0 in
Some clouds this evening will give way to mainly clear skies overnight. Low 17F. Winds NW at 10 to 20 mph."

http://www.wunderground.com/weather-forecast/zmw:20102.7.99999

The second link shows various measurements, such as wind velocity, temperature, humidity, taken several times per hour. This data can be saved in comma-delimited format.
http://www.wunderground.com/history/airport/KIAD/2015/1/27/DailyHistory.html?req_city=Dulles+Airport&req_state=VA&req_statename=Virginia

According to the NASA scheme, the first link is an example of Level 4 or 5 abstraction, while the second link is probably an example of Level 1, 2, or 3 abstraction.