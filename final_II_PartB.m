urlbase = 'http://mag.gmu.edu/git-data/cds302/';
for i = 2:3
    
    fname = sprintf('bou2013040%dvmin.min',i);
    url = sprintf('%s/%s',urlbase,fname);
    fprintf('Downloading and saving %s ...',fname);
    system(sprintf('curl -O %s',url));
    fprintf('Done\n');
    
    fprintf('Reading %s ...',fname);
    fid=fopen(fname);
    k = 1;
    while 1
        tline = fgetl(fid);
        if ~ischar(tline),break;end
        if strmatch('2013-',tline)
            tline = regexprep(tline,'-',' ');
            tline = regexprep(tline,':',' ');
            % Convert line to array and place array in row of matrix.
            A(k,:) = str2num(tline);
            k = k+1;
        end
    end
    fclose(fid);
    fprintf('Done.\n');

    % See note in solutions for why this is may not the best way to create the minute
    % array (data may be missing).
    B = horzcat([0:1439]', A(:,8)); %Create desired matrix
    
    fname2 = sprintf('bou2013040%dvmin.txt',i);
    fprintf('Saving %s ...',fname2);
    
    dlmwrite(fname2,B,'precision','%.2f');
    
    fprintf('Done.\n');
    
end
