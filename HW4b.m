clear;
A = randi([0 2^16-1],10^5,1);
                                 
tic();                                                                          
fid = fopen('file.bin','wb');                                                   
fwrite(fid,A,'double');                                                         
fclose(fid);                                                                    
stop = toc();                                                                                                                          
fprintf('Time [s] to write binary = %.8f.  Size [bytes] = %d\n',stop);