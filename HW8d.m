%Quick and dirty plots

clearvars -except data dates
A = importdata('average_intensity.txt')';

for i = 3964:3964+364
    B(i-3963) = data(i,5);
end

%Ignore Nans for purposes of mean and standard deviation (used in place of nanmean and nanstd functions)
I = find(isnan(B)==0);
m = mean(B(I));
s = std(B(I));

%Normalize data
A_norm = (A - mean(A))/std(A);
B_norm = (B - m)/s;


plot(A_norm,'r-')
hold on;
plot(B_norm,'b-')
title('Data graphed over year')
xlabel('Days')
ylabel('Normalized Intensity (unitless)')
legend('Image intensity values','Total irradiance values')