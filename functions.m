function out=normalize(A)
I = find(isnan(A)==0);
m = mean(A(I));
s = std(A(I));
out = (A - m)/s;

function [f,Y] = nanfft(B)
A = naninterp(B); %Interpolate over missing data values
Y = fft(A);
Y = abs(Y);
N = length(Y);
f = [-N/2:N/2]/N;
Y(1) = mean(Y);
Y = Y(1:N/2);
f=[1:N/2]/N;



%Taken from http://www.mathworks.com/matlabcentral/fileexchange/8225-naninterp
function X = naninterp(X) 
% Interpolate over NaNs 
X(isnan(X)) = interp1(find(~isnan(X)), X(~isnan(X)), find(isnan(X)), 'PCHIP'); 
return

%movingmean() was used in place of filter(); taken from http://www.mathworks.com/matlabcentral/fileexchange/41859-moving-average-function