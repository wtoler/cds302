#Timing this using "time bash final_IIparallel.sh" took only .003 secs!
#This is because that is how long it took to submit both jobs and return to the prompt.
#This is why code was used to test for the existence of file that is created when the job is finished.
octave --silent --eval "final_IIa" > out1.txt &
octave --silent --eval "final_IIb" > out2.txt &