import csv

print 'PRAGMA foreign_keys=OFF;'
print 'BEGIN TRANSACTION;'
print 'CREATE TABLE REPEATER (CPU REAL, MEM REAL, COMMAND text);'

with open('repeater.log','r') as csvfile:
     contents = csv.reader(csvfile, delimiter=',')
     for row in contents:
         print "INSERT INTO REPEATER (CPU, MEM, COMMAND) VALUES ('" + row[0] + "','" + row[1] + "','" + row[2] + "');"

print 'COMMIT;'